Feature: Example

  Scenario: Search for developer diploma
    Given we are on the CESI home page
    When we go on the page named "Informatique & Numérique" for the category "Domaines"
    And we search for "développeur"
    Then we found diploma with title "Concepteur·trice développeur·se d’applications"
