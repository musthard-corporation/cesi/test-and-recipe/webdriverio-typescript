import { Given, When, Then } from '@wdio/cucumber-framework';
import { expect, $ } from '@wdio/globals'

Given('we are on the CESI home page', async () => {
    await browser.maximizeWindow();
    await browser.url(`https://www.cesi.fr/`);
    if (await $('#didomi-popup').isExisting()) {
        console.log('> Popup open');
        await $('#didomi-popup .didomi-popup-container .didomi-popup-view .didomi-continue-without-agreeing').click();
    }
});

When('we go on the page named {string} for the category {string}', async (page, category) => {
    await $(`//header[contains(@id, "menu")]//div[contains(@id, "main-nav")]//nav//ul[contains(@id, "menu-menu-marque-cesi")]/li/button[.//*[contains(text(), "${category}")]]`).click();
    await $(`//header[contains(@id, "menu")]//div[contains(@id, "main-nav")]//nav//ul[contains(@id, "menu-menu-marque-cesi")]/li[button//*[contains(text(), "${category}")]]/ul/li/a[.//*[contains(text(), "${page}")]]`).click();
});

When('we search for {string}', async (search) => {
    await $('.main-search form input#main-search__input-field').setValue(search);
    await $('.main-search form button').click();
});

Then('we found diploma with title {string}', async (title) => {
    await expect(await $(`//main//div[contains(@class, "search-results")]//ul/li//h2//*[text()="${title}"]`)).toBeDisplayed();
});
